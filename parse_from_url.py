import urllib
import json

url = """
http://www.ranker.com/list/full-list-of-volvo-models/reference?var=12&utm_expid=16418821-253.BAWaw7_5T3qAVTf7EdWDtA.1&page=3&utm_referrer=http%3A%2F%2Fwww.ranker.com%2Flist%2Ffull-list-of-volvo-models%2Freference%3Fvar%3D12%26page%3D2



"""
file_name = 'volvo3.json'
MAKE_ID = 13


f = urllib.urlopen(url)
html_doc = f.read()
class Car:
	def __init__(self):
		self.name = ''
		self.typical = ''
		self.description = ''
		self.img = ''
from bs4 import BeautifulSoup
soup = BeautifulSoup(html_doc, 'html.parser')
list_ele = soup.select("#listBody #mainListCnt ol > li.blog ")
cars = []

for ele_tag in list_ele:
	car = Car()
	car_name = ele_tag.select('.name > div > h2 > a')
	if car_name:
		car_name = car_name[0].string
		car_name = (car_name[1:])


	car_info_ele = ele_tag.select('.float > .blogText')
	if car_info_ele:
		car_info_ele = car_info_ele[0]
		car_description = car_info_ele.select('.blogTextWiki')

		if car_description:
			car_description = car_description[0].get_text()
			car_description = unicode(car_description[1:])
		else:
			car_description = ''
		car_class = car_info_ele.select('.propText > ul > li')
		if car_class and len(car_class) > 1:
			car_class = car_class[1].get_text()
			car_class = car_class[7:]
			#car_class = str(car_class)
		else: 
			car_class = ''

	car_img = ''
	car_img_ele = ele_tag.select('.float > div > figure > span > a > img')
	if car_info_ele and len(car_img_ele) > 0:
		car_img = car_img_ele[0]['src']

	if car_img == '':
		car_img_ele = ele_tag.select('.float > .floatRight > figure > span > img')
		if car_info_ele and len(car_img_ele) > 0:
			car_img = car_img_ele[0]['data-img']

	car = Car()
	if  car_name != '':
		car.name = car_name or ''
		car.typical = car_class or ''
		car.img = car_img or ''
		car.description = car_description or ''

		cars.append(car)

print "Length cars", len(cars)


with open(file_name, 'w') as f:
	f.write('[\n')
	i = 0
	for car in cars:
		i = i+1
		json.dump({"model": "cars.carmodel", "fields": {"name": car.name, "make": MAKE_ID, "type": car.typical , "photo": car.img, "description": car.description }, }, f, indent = 2)
		if i < len(cars):
			f.write(',\n')
	f.write('\n]')
print "List length: ", len(cars)
print car.name
print file_name


