import urllib
import json

html_file = "Mobile Electronics - Huge Selection & FREE SHIPPING!.html"
CATALOG_TYPE = 'O' 
CATALOG_ID = 78
file_name = 'o' + str(CATALOG_ID) + '.json'



with open(html_file, 'r') as f:
	html_doc = f.read()

class Product:
	def __init__(self):
		self.name = ''
		self.cost = ''
		self.photo = ''
		self.quantum = 10
		self.created = '2016-10-18T01:46:35.797Z'
		self.updated = '2016-10-18T01:46:35.797Z'
		
from bs4 import BeautifulSoup
soup = BeautifulSoup(html_doc, 'html.parser')
list_ele = soup.select(".productGridContainer .productList ul.productItem")

ele_catalog_name = soup.select(".pageFrame .pageBodyWrapper .pageBody .departmentContent .departmentTitle")
if ele_catalog_name:
	catalog_name = ele_catalog_name[0].string
print len(list_ele)
products = []

for ele in list_ele:
	product = Product()
	ele_photo = ele.select("li h2 a img.productImage")
	if ele_photo:
		product.photo = ele_photo[0]['src']
	ele_name = ele.select("li.productLink h2 a")
	if ele_name:
		product.name = ele_name[0].string
	ele_cost = ele.select("li.priceLine label.productPrice ")
	if ele_cost:
		product.cost = ele_cost[0].string
	if product.name != '':
		products.append(product)
	print product.name, product.photo, product.cost

print len(products)


with open(file_name, 'w') as f:
	f.write('[\n')
	i = 0
	json.dump({"model": "products.productcatalog", "fields": {"id": CATALOG_ID, "type": CATALOG_TYPE , "name": catalog_name }, }, f, indent = 2)
	f.write(',\n')
	for product in products:
		i = i+1
		json.dump({"model": "products.product", "fields": {"created": product.created, "updated": product.updated, "catalog": CATALOG_ID , "name": product.name, "photo": product.photo, "cost": product.cost, "quantum": product.quantum }, }, f, indent = 2)
		if i < len(products):
			f.write(',\n')
	f.write('\n]')

print product.name
print file_name


