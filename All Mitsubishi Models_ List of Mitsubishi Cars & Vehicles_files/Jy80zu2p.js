



/* ControlTag Loader for Ranker aa2a43e6-8589-4ce3-ac01-2ada876a669b */
(function(w, cs) {
  
  if (/Twitter for iPhone/.test(w.navigator.userAgent || '')) {
    return;
  }

  var debugging = /kxdebug/.test(w.location);
  var log = function() {
    
    debugging && w.console && w.console.log([].slice.call(arguments).join(' '));
  };

  var load = function(url, callback) {
    log('Loading script from:', url);
    var node = w.document.createElement('script');
    node.async = true;  
    node.src = url;

    
    node.onload = node.onreadystatechange = function () {
      var state = node.readyState;
      if (!callback.done && (!state || /loaded|complete/.test(state))) {
        log('Script loaded from:', url);
        callback.done = true;  
        callback();
      }
    };

    
    var sibling = w.document.getElementsByTagName('script')[0];
    sibling.parentNode.insertBefore(node, sibling);
  };

  var config = {"app":{"name":"krux-scala-config-webservice","version":"3.17.0","schema_version":3},"confid":"Jy80zu2p","context_terms":[],"publisher":{"id":1808,"name":"Ranker","uuid":"aa2a43e6-8589-4ce3-ac01-2ada876a669b","version_bucket":"stable","version_hash":"4ee828218d9f71debc118d74904a389d"},"params":{"link_header_bidder":false,"site_level_supertag_config":"site","recommend":false,"control_tag_pixel_throttle":100,"fingerprint":false,"user_data_timing":"load","store_realtime_segments":false,"tag_source":false,"link_hb_start_event":"ready","first_party_uid":false,"link_hb_timeout":2000,"link_hb_adserver_subordinate":true,"optimize_realtime_segments":false,"link_hb_adserver":"dfp","target_fingerprint":false,"context_terms":false,"dfp_premium":true},"prioritized_segments":[],"realtime_segments":[],"services":{"userdata":"//cdn.krxd.net/userdata/get","contentConnector":"//connector.krxd.net/content_connector","stats":"//apiservices.krxd.net/stats","optout":"//cdn.krxd.net/userdata/optout/status","event":"//beacon.krxd.net/event.gif","set_optout":"//apiservices.krxd.net/consumer/optout","data":"//beacon.krxd.net/data.gif","link_hb_stats":"//beacon.krxd.net/link_bidder_stats.gif","userData":"//cdn.krxd.net/userdata/get","link_hb_mas":"//link.krxd.net/hb","config":"//cdn.krxd.net/controltag/{{ confid }}.js","social":"//beacon.krxd.net/social.gif","addSegment":"//cdn.krxd.net/userdata/add","pixel":"//beacon.krxd.net/pixel.gif","um":"//apiservices.krxd.net/um","click":"//apiservices.krxd.net/click_tracker/track","stats_export":"//beacon.krxd.net/controltag_stats.gif","cookie":"//beacon.krxd.net/cookie2json","proxy":"//cdn.krxd.net/partnerjs/xdi","is_optout":"//beacon.krxd.net/optout_check","impression":"//beacon.krxd.net/ad_impression.gif","transaction":"//beacon.krxd.net/transaction.gif","log":"//jslog.krxd.net/jslog.gif","set_optin":"//apiservices.krxd.net/consumer/optin","usermatch":"//beacon.krxd.net/usermatch.gif"},"site":{"id":23897,"name":"Ranker.com"},"tags":[{"id":19318,"name":"MediaMath User Match","content":"<script>\n\n(function(){\n\tvar prefix = window.location.protocol == 'https:' ? 'https:' : 'http:';\n\tvar url = prefix + '//pixel.mathtag.com/sync/img?redir=' + prefix + '%2F%2Fbeacon.krxd.net%2Fusermatch.gif%3Fpartner%3Dmediamath%26mmuuid%3D%5BMM_UUID%5D';\n\t(new Image()).src = url;\n})();\n\n</script>","target":null,"target_action":"append","timing":"asap","method":"iframe","internal":true,"template_replacement":true,"criteria":["and",["and",["and",["<=","$frequency",3]]]]},{"id":19320,"name":"AppNexus User Match","content":"<script>\n(function(){\n        var kuid = Krux('get', 'user');\n        if (kuid) {\n            var prefix = location.protocol == 'https:' ? \"https:\" : \"http:\";\n            var kurl = prefix + '//beacon.krxd.net/usermatch.gif?adnxs_uid=$UID';\n            var appnexus_url = '//ib.adnxs.com/getuid?' + kurl\n            var i = new Image();\n            i.src = appnexus_url;\n        }\n})();\n</script>","target":null,"target_action":"append","timing":"asap","method":"document","internal":true,"template_replacement":true,"criteria":["and",["and",["and",["<=","$frequency",3]]]]},{"id":19337,"name":"TubeMogul User Match","content":"<script>\n    (function() {\n        var prefix = location.protocol == 'https:' ? \"https\" : \"http\";\n        var tm_url = prefix + '://rtd.tubemogul.com/upi/pid/NC4WTmcy?redir=' + prefix + '%3A%2F%2Fbeacon.krxd.net%2Fusermatch.gif%3Fpartner_id%3Dcb276571-e0d9-4438-9fd4-80a1ff034b01%26puid%3D%24%7BTM_USER_ID%7D'\n        var i = new Image();\n        i.src = tm_url;\n    })();\n</script>","target":null,"target_action":"append","timing":"asap","method":"document","internal":true,"template_replacement":true,"criteria":["and",["and",["and",["<=","$frequency",3]]]]},{"id":19266,"name":"Krux Track Social","content":"<script type=\"text/javascript\">Krux('social.init');</script>","target":null,"target_action":"append","timing":"onload","method":"document","internal":true,"template_replacement":true,"criteria":[]},{"id":19267,"name":"Technographic Data provider tag","content":"<script>\r\n// this tag is intentionally blank\r\n</script>","target":null,"target_action":"append","timing":"onload","method":"document","internal":true,"template_replacement":true,"criteria":["and",["and",["and",["<=","$frequency",3]]]]},{"id":19268,"name":"Krux Geographic Data provider tag","content":"None","target":null,"target_action":"append","timing":"onload","method":"document","internal":true,"template_replacement":true,"criteria":["and",["and",["and",["<=","$frequency",3]]]]},{"id":19316,"name":"DataXu User Match","content":"<script>\n(function(){\n        var kuid = Krux('get', 'user');\n        var prefix = location.protocol;\n        if (kuid) {\n           var dxu_url = '//i.w55c.net/ping_match.gif?st=Krux&rurl=' + prefix + '//beacon.krxd.net/usermatch.gif?partner=dataxu&uid=_wfivefivec_';\n           var i = new Image();\n           i.src = dxu_url;\n        }\n})();\n</script>","target":null,"target_action":"append","timing":"onload","method":"document","internal":true,"template_replacement":true,"criteria":["and",["and",["and",["<=","$frequency",3]]]]},{"id":19317,"name":"DBM User Match","content":"<script>\r\n(function() {\r\n  if (Krux('get', 'user') != null) {\r\n      new Image().src = 'https://usermatch.krxd.net/um/v2?partner=google';\r\n  }\r\n})();\r\n</script>","target":"","target_action":"append","timing":"onload","method":"document","internal":true,"template_replacement":true,"criteria":["and",["and",["and",["<=","$frequency",3]]]]},{"id":19319,"name":"TTD User Match","content":"<script>\n(function()\n{ var i = new Image(); i.src = '//match.adsrvr.org/track/cmf/generic?ttd_pid=krux&ttd_tpi=1'; }\n)();\n</script>","target":null,"target_action":"append","timing":"onload","method":"document","internal":true,"template_replacement":true,"criteria":["and",["and",["and",["<=","$frequency",3]]]]},{"id":19270,"name":"DTC-Ranker","content":"<script>\r\n(function() {\r\n\r\n\t// Using UrlPath 1 to produce page attribute url_path_1\r\n\tKrux('scrape', { 'page_attr_url_path_1': {url_path: '1'}});\r\n\t// Using UrlPath 2 to produce page attribute url_path_2\r\n\tKrux('scrape', { 'page_attr_url_path_2': {url_path: '2'}});\r\n\t// Using UrlPath 3 to produce page attribute url_path_3\r\n\tKrux('scrape', { 'page_attr_url_path_3': {url_path: '3'}});\r\n\t// Using Globals LIST.userName to produce page attribute LIST.userName\r\n\tKrux('scrape', { 'page_attr_LIST.userName': {js_global: \"LIST.userName\"}});\r\n\r\n\tvar rankerCookie = {};\r\n\tvar c = document.cookie;\r\n\tc.split( '; ' ).forEach( function( el, idx, ar ) {\r\n\t\tvar splitEl = el.split( '=' );\r\n\t\trankerCookie[splitEl[0]] = splitEl[1];\r\n\t});\r\n\r\n\tvar rawPageData = {};\r\n\tif(window.dataLayer && dataLayer[0]) {\r\n\r\n\r\n\t\tvar array = [];\r\n\t\tvar JSONstring;\r\n\t\tvar tags = unescape(dataLayer[0].allTags);\r\n\r\n\t\twhile( tags.indexOf( ']' ) != '0' ) {\r\n\t\t\tJSONstring = tags.slice( tags.indexOf( '{' ), tags.indexOf('}') + 1 );\r\n\t\t\tarray.push( JSON.parse( JSONstring ) );\r\n\r\n\t\t\ttags = tags.slice( tags.indexOf('}') + 1, tags.length );\r\n\t\t}\r\n\r\n\t\tvar tagNames = [];\r\n\t\tfor(var i=0; i<array.length; i++) {\r\n\t\t\tvar t = array[i];\r\n\t\t\tif(t.name) {tagNames.push(t.name) }\r\n\t\t}\r\n\r\n\t\trawPageData = {\r\n\t\t\t'rankerListTags': tagNames.join(','),\r\n\t\t\t'rankerCat': dataLayer[0].category,\r\n\t\t\t'rankerClass': unescape(dataLayer[0].clazz),\r\n\t\t\t'rankerListFormat': dataLayer[0].listFormat,\r\n\t\t\t'rankerAge': dataLayer[0].uag,\r\n\t\t\t'rankerGender': dataLayer[0].ugndr,\r\n\t\t\t'rankerRREF': rankerCookie._RREF,\r\n\t\t\t'rankerRREFID': rankerCookie._RREFID,\r\n\t\t\t'rankerListPage': dataLayer[0].pageNumber,\r\n\t\t\t'rankerListId': dataLayer[0].listId,\r\n\t\t\t'rankerPageType': dataLayer[0].pageType\r\n\t\t}\r\n\t}\r\n\r\n\tvar readyData = {};\r\n\r\n\tfor( var key in rawPageData ) {\r\n\t\treadyData[ 'page_attr_' + key ] = rawPageData[key];\r\n\t}\r\n\r\n\t//for USER attribute in browser cookie\r\n\tvar rawUserData = {};\r\n\tif(RNKR.user.id) {\r\n\t\trawUserData = {\r\n\t\t\t'rankerUserId': RNKR.user.id\r\n\t\t}\r\n\t}\r\n\r\n\tfor( var key in rawUserData ) {\r\n\t\treadyData[ 'user_attr_' + key ] = rawUserData[key];\r\n\t}\r\n\r\n\tKrux( 'set', readyData );\r\n})()\r\n</script>","target":"","target_action":"append","timing":"onready","method":"document","internal":true,"template_replacement":true,"criteria":[]}],"link":{"adslots":{},"bidders":{}}};
  
  for (var i = 0, tags = config.tags, len = tags.length, tag; (tag = tags[i]); ++i) {
    if (String(tag.id) in cs) {
      tag.content = cs[tag.id];
    }
  }

  
  var esiGeo = String(function(){/*
   <esi:include src="/geoip_esi"/>
   */}).replace(/^.*\/\*[^{]+|[^}]+\*\/.*$/g, '');

  if (esiGeo) {
    log('Got a request for:', esiGeo, 'adding geo to config.');
    try {
      config.geo = w.JSON.parse(esiGeo);
    } catch (__) {
      
      log('Unable to parse geo from:', config.geo);
      config.geo = {};
    }
  }



  var proxy = (window.Krux && window.Krux.q && window.Krux.q[0] && window.Krux.q[0][0] === 'proxy');

  if (!proxy || true) {
    

  load('//cdn.krxd.net/ctjs/controltag.js.4ee828218d9f71debc118d74904a389d', function() {
    log('Loaded stable controltag resource');
    Krux('config', config);
  });

  }

})(window, (function() {
  var obj = {};
  
  return obj;
})());
